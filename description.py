def description_card():
    return html.Div(
        id="description-card",
        children=[
            html.H1('US Chronic Disease Analytics Dashboard', style={'textAlign': 'center'}),
            html.H2('Overview', style={'textAlign': 'center'}),
            html.P(
                """
                This dashboard provides insights into chronic diseases in the United States.
                It allows users to explore various indicators related to chronic health conditions,
                their prevalence across different states, and trends over time.
                """,
                style={'textAlign': 'justify'}
            ),
            html.H3('Data Description', style={'textAlign': 'center'}),
            html.P(
                """
                The data used in this dashboard is sourced from the US Chronic Disease Indicators (CDI)
                database. It covers a wide range of health conditions and risk factors that are known to
                affect the population's health.
                """,
                style={'textAlign': 'justify'}
            )
            
        ]
    )
